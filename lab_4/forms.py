# from typing import Tuple
from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['untuk', 'dari', 'judul', 'pesan']
    error_messages = {
        'required' : 'Please Type'
    }
    input_untuk = {
        'type' : 'text',
        'placeholder' : 'Siapa penerima pesan ini?'
    }
    input_dari= {
        'type' : 'text',
        'placeholder' : 'Siapa pengirim pesan ini?'
    }
    input_judul = {
		'type' : 'text',
		'placeholder' : 'Apa judul pesan ini?'
	}
    input_pesan = {
        'type' : 'text',
        'placeholder' : 'Apa yang ingin disampaikan?'
    }
    untuk = forms.CharField(label='Untuk: ', required=True, 
            max_length=50, widget=forms.TextInput(attrs=input_untuk))
    dari = forms.CharField(label='Dari: ', required=True, 
            max_length=50, widget=forms.TextInput(attrs=input_dari))
    judul = forms.CharField(label='Judul: ', required=True, 
            max_length=100, widget=forms.TextInput(attrs=input_judul))
    pesan = forms.Textarea(attrs=(input_pesan))

