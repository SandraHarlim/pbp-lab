from django.urls import path
from .views import index
# from .views import add_note
# from .views import note_list

urlpatterns = [
    path('', index, name='index'),
]