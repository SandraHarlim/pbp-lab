from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    form = FriendForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid):
        form.save()
        return HttpResponseRedirect('/lab-3')
    response['form'] = form
    return render(request, "lab3_form.html", response)