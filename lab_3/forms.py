from typing import Tuple
from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
    error_messages = {
        'required' : 'Please Type'
    }
    input_name = {
        'type' : 'text',
        'placeholder' : 'Masukkan nama kamu'
    }
    input_npm = {
        'type' : 'text',
        'placeholder' : 'Masukkan NPM kamu'
    }
    input_dob = {
		'type' : 'date',
		'placeholder' : 'Masukkan Date of Birth Kamu'
	}
    name = forms.CharField(label='Name: ', required=True, 
            max_length=30, widget=forms.TextInput(attrs=input_name))
    npm = forms.CharField(label='NPM: ', required=True, 
            max_length=10, widget=forms.TextInput(attrs=input_npm))
    dob = forms.DateField(label='Date of Birth: ', required=True, 
            widget=forms.DateInput(attrs=input_dob))
