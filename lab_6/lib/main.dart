import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:select_form_field/select_form_field.dart';

// Referensi :
// https://api.flutter.dev/flutter/material/Card-class.html
// https://github.com/flutter/flutter/issues/15919
// https://gallery.flutter.dev/#/
// https://pub.dev/packages/datetime_picker_formfield
// https://flutter.dev/docs/cookbook/design/themes

final List<Map<String, dynamic>> _items = [
  {
    'value': 'Monday',
    'label': 'Monday',
    'textStyle': TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
  },
  {
    'value': 'Tuesday',
    'label': 'Tuesday',
    'textStyle': TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
  },
  {
    'value': 'Wednesday',
    'label': 'Wednesday',
    'textStyle': TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
    // 'enable': false,
  },
  {
    'value': 'Thursday',
    'label': 'Thursday',
    'textStyle': TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
  },
  {
    'value': 'Friday',
    'label': 'Friday',
    'textStyle': TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
  },
];

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'P.B.P',
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primarySwatch: Colors.teal,
        scaffoldBackgroundColor: Color.fromRGBO(235, 240, 242, 100),

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline2: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold,
              color: Color.fromRGBO(16, 130, 88, 100)),
          bodyText2: TextStyle(fontSize: 15.0, fontFamily: 'Hind'),
        ),
      ),
      home: new MyCard(),
    );
  }
}

class MyCard extends StatelessWidget{
  const MyCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      // body: const MyCostumCard(),
      appBar: AppBar(
        title: Text("Susun Jadwal"),
      ),
      body: const MyCostumCard(),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(5.0),
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.teal,
              ),
              child: Text('P.B.P'),
            ),
            ListTile(
              title: const Text('Susun Jadwal'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('To-Do List'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Recently on Education'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Pandemic Forum'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Quiz of Pandemic'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('COVID19 Data'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Scheduler'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class MyCostumCard extends StatefulWidget{
  const MyCostumCard({Key? key}) : super(key: key);

  @override
  MyCostumCardState createState(){
    return MyCostumCardState();
  }
}

class MyCostumCardState extends State<MyCostumCard>{

  @override
  Widget build(BuildContext context){
    return Center(
      child: Column(
        children: [
          Expanded(
            child: Card(
              color: Color.fromRGBO(169, 212, 217, 100),
              child: Container(
                width: 500,
                child: const ListTile(
                  title: Text('MONDAY', style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text('08.00 - 10.30 | MPPI\n13.00 - 14.40 PBP'),
                ),
              ),
            ),
          ),
          Expanded(
            child: Card(
              color: Color.fromRGBO(169, 212, 217, 100),
              child: Container(
                width: 500,
                child: const ListTile(
                  title: Text('TUESDAY', style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text('10.00 - 11.40 | SDA'),
                ),
              ),
            ),
          ),
          Expanded(
            child: Card(
              color: Color.fromRGBO(169, 212, 217, 100),
              child: Container(
                width: 500,
                child: const ListTile(
                  title: Text('WEDNESDAY', style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text('14.00 - 16.30 | ALIN'),
                ),
              ),
            ),
          ),
          Expanded(
            child: Card(
              color: Color.fromRGBO(169, 212, 217, 100),
              child: Container(
                width: 500,
                child: const ListTile(
                  title: Text('THURSDAY', style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text('10.00 - 11.40 |SDA\n13.00 - 14.40 PBP'),
                ),
              ),
            ),
          ),
          Expanded(
            child: Card(
              color: Color.fromRGBO(169, 212, 217, 100),
              child: Container(
                width: 500,
                child: const ListTile(
                  title: Text('FRIDAY', style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text('08.00 - 10.30 | SOSI'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: ElevatedButton(
                child: const Text("Tambah jadwal matkul"),
                onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyForm()));
                }
            ),
          ),
        ],
      ),
    );
  }
}

class MyForm extends StatelessWidget{
  const MyForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
          title: Text("Tambahkan mata kuliah"),
        ),
        body: const MyCostumForm(),
    );
  }
}

class MyCostumForm extends StatefulWidget{
  const MyCostumForm ({Key? key}) : super(key: key);

  @override
  MyCostumFormState createState(){
    return MyCostumFormState();
  }
}

class MyCostumFormState extends State<MyCostumForm>{
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context){
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Masukkan nama matkul",
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: TextFormField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Masukan nama matkul',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty){
                    return "Please fill out this field.";
                  }
                  return null;
                }
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Masukkan hari matkul diadakan",
                style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: SelectFormField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                ),
                type: SelectFormFieldType.dropdown,
                initialValue: "Monday",
                items: _items,
                onChanged: (val) => print(val),
                onSaved: (val) => print(val),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Masukkan pukul berapa matkul berlangsung",
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: DateTimeField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Masukan pukul berapa matkul diadakan',
              ),
              format: DateFormat("HH:mm"),
              onShowPicker: (context, currentValue) async {
                final time = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                );
                return DateTimeField.convert(time);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Masukkan pukul berapa matkul berakhir",
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: DateTimeField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Masukan pukul berapa matkul diadakan',
              ),
              format: DateFormat("HH:mm"),
              onShowPicker: (context, currentValue) async {
                final time = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                );
                return DateTimeField.convert(time);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: ElevatedButton(
                  child: const Text("Add jadwal matkul"),
                  onPressed: (){
                    if (_formKey.currentState!.validate()){
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text("Data is processing")),
                      );
                    }
                  }
              ),
            ),
          ),
        ],
      ),
    );
  }
}
