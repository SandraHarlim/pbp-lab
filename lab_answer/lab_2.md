Nama    : Sandra Harlim
NPM     : 2006464303
Kelas   : F

1. Apakah perbedaan antara JSON dan XML?
--> JSON merupakan singkatan dari JavaScript Object Nation. JSON didasarkan oleh bahasa pemrograman JavaScript. JSON pertama kali dipopulerkan oleh Douglas Crockford. JSON biasanya digunakan untuk menyimpan dan mentransfer data (Nayoan, 2020). JSON hanya mendukung encoding UTF-8. Pada JSON, biasanya data disimpan dalam bentuk dictionary pada python yang memiliki key dan value. JSON menyimpan data lebih rapi dan mudah dipahami oleh manusia. Untuk penerapan AJAX, JSON lebih cepat dan mudah daripada XML.
--> XML merupakan singkatan dari Extensible Markup Language. XML adalah bahasa markup yang dibuat oleh World Wide Web Consortium (W3C). XML diturunkan oleh Standard Generalized Markup Language (SGML). Berbeda dengan JSON, XML mendukung berbagai jenis encoding. Pada XML, data direpresentasikan dalam bentuk struktur tags, terdapat tags pembuka dan penutup. XML relatif sulit untuk dibaca oleh manusia. XML berfungsi untuk melakukan penyederhanaan proses penyimpanan dan pengiriman data (Pamungkas, 2020).
--> Contoh pemakaian JSON : Pada kode JSON, data disimpan di dalam sebuah dictionary yang berisi key dan value. Kode JSON lebih mudah dipahami oleh manusia dan lebih ringkas karena tidak membutuhkan tag pembuka dan penutup seperti XML. 
{"students":[
    {"namaDepan":"Sandra", "npm":"2006464303"},
    {"namaDepan":"Hally", "npm":"2006465301"},
    {"namaDepan":"Silvi", "npm":"2006463903"},
]}
--> Contoh pemakaian XML : Pada kode XML, kita harus mendeklarasi versi XML yang kita gunakan pada tag pertama. Kita juga dapat membuat nama tag secara bebas. Setiap tag harus selalu ditutup meskipun tag tersebut tidak memiliki isi. 
<students>
    <student>
        <namaDepan>Sandra</namaDepan> <npm>2006464303</npm>
    </student>
    <student>
        <namaDepan>Hally</namaDepan> <npm>2006465301</npm>
    </student>
    <student>
        <namaDepan>Silvi</namaDepan> <npm>2006463903</npm>
    </student>
</students>

2. Apakah perbedaan antara HTML dan XML?
--> HTML adalah singkatan dari Hypertext Markup Language. Menurut Pamungkas (2020), HTML berfungsi untuk menampilkan data. Tags pada HTMl bersifat khusus (predefined tags) berbeda dengan XML. Pada HTML, huruf kapital dan huruf kecil tidak dapat dibedakan (case insensitive). Menurut Iswara, kita tidak bisa mempertahankan spasi di HTML. Menurut Aulil (2021), jika kita ingin membuat spasi, kita bisa menggunakan kode &nbsp; jika ingin menambahkan spasi. 
--> Contoh pemakaian HTML : 
<html>
    <head>
        <title>Menampilkan identitas mahasiswa</title>
    </head>
    <body>
        <h1>Menampilkan identitas mahasiswa</h1>
        <h2>Sandra&nbsp;2006464303</h2>
        <h2>Hally&nbsp;2006465301</h2>
        <h2>Silvi&nbsp;2006463903</h2>
    </body>
</html>
--> Pada kode HTML, dapat dilihat bahwa tags tersebut merupakan tags khusus yang telah di-defined oleh HTML, berbeda dengan XML. Untuk menggunakan spasi, kita dapat menggunakan kode &nbsp;

Referensi : 
Nayoan, A. (2020, August 19). JSON: Pengertian, Fungsi dan Cara Menggunakannya. Niagahoster Blog. https://www.niagahoster.co.id/blog/json-adalah/

Aulil. (2021, March 15). Cara Membuat Spasi di HTML Dengan Mudah. Fakultas Ilmu Komputer Dan Teknologi Informasi (FIK-TI) UMSU. https://fikti.umsu.ac.id/cara-membuat-spasi-di-html-dengan-mudah/

Pamungkas, R. B. (2020, July 17). Panduan Lengkap XML: Pengertian, Contoh, dan Cara Membuka Filenya. Niagahoster Blog. https://www.niagahoster.co.id/blog/xml/#Apa_Perbedaan_XML_dengan_HTML 

Iswara, B. D. (n.d.). Perbedaan XML dan HTML: Fitur dan Kuncinya. DosenIT.com. Retrieved September 24, 2021, from https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html